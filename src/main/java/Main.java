import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        List<Vehiculo> listaVehiculos = obtenerListaVehiculos("vehiculos.txt");
        //System.out.println("cantidad motos: " + obtenerCarrosMotos(listaVehiculos, Vehiculo.EnumTipoVehiculo.MOTO).size());
        //System.out.println("cantidad carros: " + obtenerCarrosMotos(listaVehiculos, Vehiculo.EnumTipoVehiculo.CARRO).size());
        //System.out.println("agrupando por tipo: " + agruparVehiculosPorTipo(listaVehiculos));
        //calcularImpuestos(listaVehiculos).stream().forEach(v -> System.out.println("Precio: " + v.getPrecio() + ", Impuesto: " + v.getImpuesto()));
        //establecerEstadoVehiculo(listaVehiculos).stream().forEach(v -> System.out.println("Vehiculo: " + v.getMarca() + " - " + v.getModelo() + ", Estado: " + v.getEstado()));
        //System.out.println("agrupando por año fabricacion: " + agruparVehiculosPorAnnoFabricacion(listaVehiculos));
        //System.out.println("agrupando por valor actual: " + agruparVehiculosPorValorActual(listaVehiculos));
        //System.out.println("agrupando por valor tipo de cambio: " + agruparVehiculosPorTipoCambio(listaVehiculos, esCarroPredicate()));
        /*IntSummaryStatistics summaryCarros =  obtenerDatosPreciosCarros(listaVehiculos, esCarroPredicate());
        System.out.println(summaryCarros.getMin());*/
        /*IntSummaryStatistics summaryMotos =  obtenerDatosPreciosMotos(listaVehiculos);
        System.out.println(summaryMotos.getMin());*/
        /*IntSummaryStatistics summaryVehiculosPorMarca =  obtenerDatosPreciosPorMarcaVehiculo(listaVehiculos, "BMW");
        System.out.println(summaryVehiculosPorMarca.getMax());*/
        //System.out.println(clasificarColoresPorModeloSegunMarca(listaVehiculos, "BMW"));
    }

    /*
     Función para clasificar los colores disponibles por modelo, de una marca en específico.
     */
    private static Map clasificarColoresPorModeloSegunMarca(List<Vehiculo> listaVehiculos, String marca) {
        return listaVehiculos.stream()
                .filter(v -> v.getMarca().equalsIgnoreCase(marca))
                .collect(Collectors.groupingBy(v -> v.getModelo(), Collectors.groupingBy(v-> v.getColor())));
    }

    /*
     Función para obtener, dada una marca, el promedio de los precios de sus vehículos, el mas costoso y menos costoso.
     */
    private static IntSummaryStatistics obtenerDatosPreciosPorMarcaVehiculo(List<Vehiculo> listaVehiculos, String marca){
        return listaVehiculos.stream()
                .filter(v -> v.getMarca().equalsIgnoreCase(marca))
                .collect(Collectors.summarizingInt(v -> v.getPrecio().intValue()));
    }


    /*
     Función para obtener datos sobre el precio de las motos (más costosa, menos costosa, promedio, suma).
     */
    private static IntSummaryStatistics obtenerDatosPreciosMotos(List<Vehiculo> listaVehiculos){
        return listaVehiculos.stream()
                .filter(v -> v instanceof Moto)
                .collect(Collectors.summarizingInt(v -> v.getPrecio().intValue()));
    }

    /*
     Función para obtener datos sobre el precio de los carros (más costoso, menos costoso, promedio, suma).
     */
    private static IntSummaryStatistics obtenerDatosPreciosCarros(List<Vehiculo> listaVehiculos, Predicate<Vehiculo> esCarroPredicate){
        return listaVehiculos.stream()
                .filter(esCarroPredicate)
                .collect(Collectors.summarizingInt(v -> v.getPrecio().intValue()));
    }

    /*
     Función para clasificar los carros si son automáticos o no.
     */
    private static Map<Boolean, List<Vehiculo>> agruparVehiculosPorTipoCambio(List<Vehiculo> listaVehiculos, Predicate<Vehiculo> esCarroPredicate){
        return listaVehiculos.stream()
                .filter(esCarroPredicate)
                .collect(Collectors.partitioningBy(v -> ((Carro)v).getTransmisión().equals("automatico")));
    }

    /*
     Función para obtener los vehículos clasificados por su valor actual en mercado (al precio restarle el 10% por año).
     */
    private static Map<Integer, List<Vehiculo>> agruparVehiculosPorValorActual(List<Vehiculo> listaVehiculos){
        return listaVehiculos.stream().collect(Collectors.groupingBy(v -> {
            LocalDate fechaActual = LocalDate.now();
            Integer annosFabricacion = fechaActual.getYear() - v.getAnno();
            Integer  precio = (int) (v.getPrecio() - (v.getPrecio() * (annosFabricacion * 0.1)));

            return precio;
        }));
    }

    /*
     Función para obtener los vehículos clasificados según el año de fabricación.
     */
    private static Map<Integer, List<Vehiculo>> agruparVehiculosPorAnnoFabricacion(List<Vehiculo> listaVehiculos){
        return listaVehiculos.stream().collect(Collectors.groupingBy(v -> v.getAnno()));
    }

    /*
     Función para establecer el estado del vehículo ('Ok', 'Averiado'). Usar aleatorios.
     */
    private static List<Vehiculo> establecerEstadoVehiculo(List<Vehiculo> listaVehiculos){
        Supplier<String> generadorEstado = () -> {
            int b = (int)(Math.random()*(1-0+1)+0);
            return b == 0? "Averiado": "Ok";
        };

        return listaVehiculos.stream()
                .map(v -> {
                    v.setEstado(generadorEstado.get());
                    return v;
                })
                .collect(Collectors.toList());
    }

    /*
     Función para calcular impuesto del vehículo (46.5%) y guardarlo en el mismo objeto
     */
    private static List<Vehiculo> calcularImpuestos(List<Vehiculo> listaVehiculos){
        UnaryOperator<Vehiculo> calculadorImpuesto = (v) -> {
            v.setImpuesto(0.0465 * v.getPrecio());
            return v;
        };

        return listaVehiculos.stream()
                .map(calculadorImpuesto)
                .collect(Collectors.toList());
    }

    /*
     Función para retornar un mapa con dos claves (Carro, Moto) según el tipo de vehículo.
     */
    private static Map<Vehiculo.EnumTipoVehiculo, List<Vehiculo>> agruparVehiculosPorTipo(List<Vehiculo> listaVehiculos){
        return listaVehiculos.stream()
                .collect(Collectors.groupingBy(v -> v.getTipo()));
    }

    /*
     Función para retornar una lista de Carros o  de motos
     */
    private static List<Vehiculo> obtenerCarrosMotos(List<Vehiculo> listaVehiculos, Vehiculo.EnumTipoVehiculo tipoVehiculo){
        return listaVehiculos.stream()
                .filter(v -> v.getTipo().equals(tipoVehiculo))
                .collect(Collectors.toList());
    }

    /*
     Leer archivo Vehiculos.txt
     Generar objetos a partir de lo leído en el archivo, aplicar concepto de herencia y retornar lista de vehículos
     */
    private static List<Vehiculo> obtenerListaVehiculos(String nombreArchivo) throws IOException {
        FileReader fileReader = new FileReader("src/main/resources/" + nombreArchivo);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Stream<String> lines = bufferedReader.lines();

        return lines
                .filter(l -> l.length() > 6)
                .map(l -> {
                    String[] lista = l.split(",");
                    if(lista.length == 7){
                        return new Carro(lista[0].trim(), lista[1].trim(), lista[5].trim(), Integer.parseInt(lista[2].trim()), Double.parseDouble(lista[3].trim()), lista[6].trim(), Integer.parseInt(lista[4].trim()));
                    }else{
                        return new Moto(lista[0].trim(), lista[1].trim(), lista[5].trim(), Integer.parseInt(lista[2].trim()), Double.parseDouble(lista[4].trim()), Integer.parseInt(lista[3].trim()));
                    }
                })
                .collect(Collectors.toList());
    }

    private static Predicate<Vehiculo> esCarroPredicate(){
        return v -> v instanceof Carro;
    }
}
