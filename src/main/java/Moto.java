public class Moto extends Vehiculo{
    private Integer cilindraje;

    public Moto(String marca, String modelo, String color, Integer anno, Double precio, Integer cilindraje) {
        super(EnumTipoVehiculo.MOTO, marca, modelo, color, anno, precio);
        this.cilindraje = cilindraje;
    }
}
