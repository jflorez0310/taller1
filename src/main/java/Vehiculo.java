public class Vehiculo {
    private EnumTipoVehiculo tipo;
    private String marca;
    private String modelo;
    private String color;
    private String estado;
    private Integer anno;
    private Double precio;
    private Double impuesto;

    public enum EnumTipoVehiculo {
        CARRO,
        MOTO
    }

    public Vehiculo(EnumTipoVehiculo tipo, String marca, String modelo, String color, Integer anno, Double precio) {
        this.tipo = tipo;
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.anno = anno;
        this.precio = precio;
    }

    public EnumTipoVehiculo getTipo() {
        return tipo;
    }

    public void setTipo(EnumTipoVehiculo tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Double impuesto) {
        this.impuesto = impuesto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
