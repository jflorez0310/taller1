public class Carro extends Vehiculo{
    private String transmisión;
    private Integer puertas;

    public Carro(String marca, String modelo, String color, Integer anno, Double precio, String transmisión, Integer puertas) {
        super(EnumTipoVehiculo.CARRO, marca, modelo, color, anno, precio);
        this.puertas = puertas;
        this.transmisión = transmisión;
    }

    public String getTransmisión() {
        return transmisión;
    }

    public void setTransmisión(String transmisión) {
        this.transmisión = transmisión;
    }

    public Integer getPuertas() {
        return puertas;
    }

    public void setPuertas(Integer puertas) {
        this.puertas = puertas;
    }
}
